package ru.pisarev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.api.ITaskRestEndpoint;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody final Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody final List<Task> tasks) {
        repository.saveAll(tasks);
        return tasks;
    }

    @Override
    @PutMapping("/save")
    public Task save(@RequestBody final Task task) {
        repository.save(task);
        return task;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody final List<Task> tasks) {
        repository.saveAll(tasks);
        return tasks;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        repository.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        repository.removeAll();
    }

}
