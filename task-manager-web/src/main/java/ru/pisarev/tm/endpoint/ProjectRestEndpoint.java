package ru.pisarev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.api.IProjectRestEndpoint;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") final String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody final Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody final List<Project> projects) {
        repository.saveAll(projects);
        return projects;
    }

    @Override
    @PutMapping("/save")
    public Project save(@RequestBody final Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody final List<Project> projects) {
        repository.saveAll(projects);
        return projects;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        repository.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        repository.removeAll();
    }

}
