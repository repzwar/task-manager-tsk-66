package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.service.ILoggingService;
import ru.pisarev.tm.dto.LoggerDTO;

import java.io.File;
import java.io.FileWriter;

import static ru.pisarev.tm.constant.LogFileName.*;

@Service
public class LoggingService implements ILoggingService {

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final LoggerDTO message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull File file = new File(fileName);
        file.getParentFile().mkdir();
        @NotNull final FileWriter fileOutputStream = new FileWriter(file, true);
        fileOutputStream.write("\"" + message.getId() + ":" + message.getType() + " " + message.getFormattedDate() + "\"\n");
        fileOutputStream.write(message.getEntity());
        fileOutputStream.close();
    }

    @Nullable
    private String getFileName(@Nullable final String className) {
        if (className == null) return null;
        switch (className) {
            case "SessionRecord":
            case "SessionGraph":
                return SESSION_LOG;
            case "UserRecord":
            case "UserGraph":
                return USER_LOG;
            case "TaskRecord":
            case "TaskGraph":
                return TASK_LOG;
            case "ProjectRecord":
            case "ProjectGraph":
                return PROJECT_LOG;
            default:
                return null;
        }
    }
}
